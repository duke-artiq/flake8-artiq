{
  pkgs ? import <nixpkgs> { },
}:

pkgs.python3Packages.callPackage ./derivation.nix { }
