{
  pkgs ? import <nixpkgs> { },
}:

let
  flake8-artiq = import ./default.nix { inherit pkgs; };
in
(pkgs.python3.withPackages (ps: [ flake8-artiq ])).env
