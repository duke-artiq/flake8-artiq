{
  buildPythonPackage,
  nix-gitignore,
  lib,
  setuptools,
  flake8,
  pytestCheckHook,
  version ? "8",
}:

buildPythonPackage {
  pname = "flake8-artiq";
  inherit version;
  src = nix-gitignore.gitignoreSource [
    "*.nix"
    "/*.lock"
  ] ./.;
  format = "pyproject";

  nativeBuildInputs = [ setuptools ];

  propagatedBuildInputs = [ flake8 ];

  checkInputs = [ pytestCheckHook ];

  condaDependencies = [ "flake8>=4.0.1" ];

  meta = with lib; {
    description = "Flake8 plugin for ARTIQ code";
    maintainers = [ "Duke University" ];
    homepage = "https://gitlab.com/duke-artiq/flake8-artiq";
    license = licenses.mit;
  };
}
