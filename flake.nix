{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.05";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs =
    {
      self,
      nixpkgs,
      flake-utils,
    }:
    flake-utils.lib.eachDefaultSystem (
      system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
        flake8-artiq = pkgs.python3Packages.callPackage ./derivation.nix {
          version = "${builtins.toString self.sourceInfo.lastModifiedDate or 0}+${
            self.sourceInfo.shortRev or "unknown"
          }";
        };
      in
      {
        packages = {
          inherit flake8-artiq;
          default = pkgs.python3.withPackages (_: [ flake8-artiq ]);
        };

        devShells = {
          default = pkgs.mkShell {
            packages = [
              (pkgs.python3.withPackages (
                ps:
                [
                  # Packages required for testing
                  ps.autopep8
                  ps.pytest
                  ps.coverage
                  ps.mypy
                  ps.flake8
                ]
                ++ flake8-artiq.propagatedBuildInputs
              ))
            ];
          };
        };

        formatter = pkgs.nixfmt-rfc-style;
      }
    );
}
