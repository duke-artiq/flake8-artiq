# Flake8 ARTIQ plugin

This repository contains an AST based [flake8](https://flake8.pycqa.org) plugin which performs additional checks
on [ARTIQ](https://github.com/m-labs/ARTIQ) code. The main goal of the plugin is to find code affected by known ARTIQ
compiler bugs and potential ambiguous kernel code. Additionally, the plugin will flag a couple of other basic bugs in
ARTIQ code. Error codes from this plugin have the format `ATQxxx` and a list of error codes can be found
in [this file](flake8_artiq/errors.py).

## Installation

The flake8-artiq plugin requires Python 3.5.3+ and is available on the **M-labs Nix and Conda channels**.
Besides, it is also possible to install flake8-artiq from source.

### Nix Flakes

When using Nix Flakes, a shell with the flake8-artiq package can be directly opened from the terminal.

```shell
nix shell gitlab:duke-artiq/flake8-artiq
```

The repository can also be added as an input in your own Nix Flake.

```nix
{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.05";
    flake8-artiq = {
      url = "gitlab:duke-artiq/flake8-artiq";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = {
    # Package available as flake8-artiq.packages.${system}.flake8-artiq
  };
}
```

### Nix installation (M-labs channels)

Instructions for setting up the M-labs Nix and Conda channels can be found in the
[ARTIQ installation manual](https://www.m-labs.hk/artiq/manual-legacy/installing.html).
The following Nix shell example script describes a Nix environment with flake8-artiq using the M-labs Nix channel.

```nix
let
  pkgs = import <nixpkgs> { };
  # We assume the M-labs Nix channel is already configured
  artiq-full = import <artiq-full> { inherit pkgs; };
in
pkgs.mkShell {
  buildInputs = [
    (pkgs.python3.withPackages (ps: [
      artiq-full.flake8-artiq
    ]))
  ];
}
```

The following command creates a Conda environment with flake8-artiq:

```shell
conda create -n f8a flake8-artiq
```

### Nix installation (source)

With Nix, it is possible to build the package directly from source in your Nix shell.

```nix
let
  pkgs = import <nixpkgs> { };
  flake8-artiq = pkgs.callPackage (import (builtins.fetchGit "https://gitlab.com/duke-artiq/flake8-artiq.git")) { inherit pkgs; };
in
pkgs.mkShell {
  buildInputs = [
    (pkgs.python3.withPackages (ps: [
      flake8-artiq
    ]))
  ];
}
```

It is also possible to build the flake8-artiq Nix package using the Nix scripts in the repository.

```shell
git clone https://gitlab.com/duke-artiq/flake8-artiq.git
cd flake8-artiq/
nix-shell
```

### Conda (source)

The flake8-artiq package can be installed in a Conda environment or venv using pip.

```shell
pip install git+https://gitlab.com/duke-artiq/flake8-artiq
```

## Usage

When the flake8-artiq plugin is installed, it is automatically recognized and used by flake8. A successful installation
of flake8-artiq can be verified by printing the flake8 version.

```shell
$ flake8 --version
7.1.0 (flake8-artiq: 8, mccabe: 0.7.0, pycodestyle: 2.12.0, pyflakes: 3.2.0) CPython 3.12.4 on Linux
```

Instructions on how to use flake8 can be found on the [flake8 website](https://flake8.pycqa.org). To only see errors
found by the flake8-artiq plugin, use the `--select` option of flake8.

```shell
flake8 --select=ATQ
```

### Options

The following options are available for flake8-artiq:

- `--implicit-rpc` Consider every function that is not decorated with an ARTIQ decorator an RPC function. This option
  can cause many false-positives and tends to be not very useful. To make sure known synchronous RPC functions are
  checked by flake8-artiq, decorate them explicitly using `@rpc`.
- `--no-untyped-args` Do not allow untyped arguments for kernel and portable functions.
- `--no-untyped-return` Do not allow untyped return value for kernel, portable, and RPC functions.

## Testing

The code in this repository can be tested by executing the following commands in the root directory of the project.

```shell
pytest  # Requires pytest to be installed
mypy    # Requires mypy to be installed
flake8  # Requires flake8 to be installed
```

## Main contributors

- Leon Riesebos (formerly Duke University)
