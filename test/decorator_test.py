import itertools

from flake8_artiq.errors import Error

from .tools import evaluate

_decorators = ['kernel', 'portable', 'rpc', 'host_only']


def test_decorator() -> None:
    decorators = [
        'kernel', 'kernel(flags={"fast-math"})',
        'portable', 'portable(flags={"fast-math"})',
        'rpc', 'rpc(flags={"async"})',
        'host_only',
    ]

    source = """
    @{}
    def foo(self):
        pass
    """

    for d in decorators:
        s = source.format(d)
        assert not evaluate(s)


def test_multiple_decorators() -> None:
    source = """
    @{}
    @{}
    def foo(self):
        pass
    """

    for d0, d1 in itertools.product(_decorators, _decorators):
        assert (4, 0, Error.MULTIPLE_DECORATORS) in evaluate(source.format(d0, d1))


def test_combined_decorators() -> None:
    regular_decorators = ['classmethod', 'staticmethod']
    source = """
    @{}
    @{}
    def foo(self):
        pass
    """

    for d0, d1 in itertools.product(_decorators, regular_decorators):
        assert not evaluate(source.format(d0, d1))
