from flake8_artiq.errors import Error

from .tools import evaluate

_compiled_decorators = ['kernel', 'portable']
_rpc_decorators = ['rpc', 'rpc(flags={"async"})']
_decorators = _compiled_decorators + _rpc_decorators
_artiq_types = ['TNone', 'TTuple([TBool, TBool])',
                'TBool', 'TInt32', 'TInt64', 'TFloat',
                'TStr', 'TBytes', 'TByteArray',
                'TList(TInt32)', 'TArray(TFloat)', 'TRange32', 'TRange64',
                'TVar']
_py_types = ['bool', 'int', 'str', 'np.int32', 'typing.List[int]', 'typing.Tuple[int, int]', 'None']
_types = _artiq_types + _py_types


def test_arg_typing() -> None:
    source = """
    @{}
    def foo(self, bar: {}):
        pass
    """
    for d in _compiled_decorators:
        for t in _artiq_types:
            assert not evaluate(source.format(d, t))
            assert not evaluate(source.format(d, t), no_untyped_args=True)
        for t in _py_types:
            assert (3, 0, Error.ARG_TYPING) in evaluate(source.format(d, t))
            assert (3, 0, Error.ARG_TYPING) in evaluate(source.format(d, t),  no_untyped_args=True)
    for d in _rpc_decorators:
        for t in _types:
            assert not evaluate(source.format(d, t))
            assert not evaluate(source.format(d, t), no_untyped_args=True)


def test_arg_typing_no_untyped_args() -> None:
    source = [
        """
        @{}
        def foo(self, bar{}):
            pass
        """,
        """
        @{}
        def foo(bar{}):
            pass
        """,
    ]
    for s in source:
        for d in _decorators:
            assert not evaluate(s.format(d, ''))
            for t in _artiq_types:
                assert not evaluate(s.format(d, ': {}'.format(t)))
                assert not evaluate(s.format(d, ': {}'.format(t)), no_untyped_args=True)
        for d in _compiled_decorators:
            assert (3, 0, Error.UNTYPED_ARGS) in evaluate(s.format(d, ''), no_untyped_args=True)
        for d in _rpc_decorators:
            assert not evaluate(s.format(d, ''), no_untyped_args=True)


def test_ret_typing() -> None:
    source = """
    @{}
    def foo(self) -> {}:
        pass
    """
    for d in _decorators:
        for t in _artiq_types:
            assert not evaluate(source.format(d, t))
        for t in _py_types:
            assert (3, 0, Error.RETURN_TYPING) in evaluate(source.format(d, t))


def test_ret_typing_no_untyped_return() -> None:
    source = """
    @{}
    def foo(self) {}:
        return {}
    """
    for d in _decorators:
        for r in ['', '1']:
            for t in _artiq_types:
                assert not evaluate(source.format(d, '-> {}'.format(t), r))
                assert not evaluate(source.format(d, '-> {}'.format(t), r), no_untyped_return=True)
                assert not evaluate(source.format(d, '', r))

        assert (4, 4, Error.UNTYPED_RETURN) in evaluate(source.format(d, '', '1'), no_untyped_return=True)
        assert not evaluate(source.format(d, '', 'None'), no_untyped_return=True)


def test_arg_typing_implicit_rpc() -> None:
    source = """
    def foo(self, bar: {}):
        pass
    """
    for t in _types:
        assert not evaluate(source.format(t), implicit_rpc=True)


def test_ret_typing_implicit_rpc() -> None:
    source = """
    def foo(self) -> {}:
        pass
    """
    for t in _artiq_types:
        assert not evaluate(source.format(t), implicit_rpc=True)
    for t in _py_types:
        s = source.format(t)
        assert (2, 0, Error.RETURN_TYPING) in evaluate(s, implicit_rpc=True)
        assert not evaluate(s, implicit_rpc=False)


def test_typing_comment() -> None:
    source = """
    @{}
    def foo(self, bar):  # type: ({}) -> None
        pass
    """
    for d in _decorators:
        for t in _types:
            # Type comments are ignored, so no error is raised
            assert not evaluate(source.format(d, t))


def test_incorrect_type_use_arg() -> None:
    source = """
    @{}
    def foo(self, bar: {}):
        pass
    """
    incorrect_types = ['TList', 'TArray', 'TTuple']
    incorrect_types_with_call = ['TList()', 'TArray()', 'TTuple()']

    for d in _compiled_decorators:
        for t in _artiq_types:
            assert not evaluate(source.format(d, t))
        for t in incorrect_types:
            assert (3, 0, Error.INCORRECT_TYPE_USE) in evaluate(source.format(d, t))
        for t in incorrect_types_with_call:
            # These types are incorrect, but we decided not to go further into type checking
            assert not evaluate(source.format(d, t))

    for d in _rpc_decorators:
        for t in _types + incorrect_types + incorrect_types_with_call:
            # RPC argument typing is not checked
            assert not evaluate(source.format(d, t))


def test_incorrect_type_use_return() -> None:
    source = """
    @{}
    def foo(self, bar) -> {}:
        pass
    """
    incorrect_types = ['TList', 'TArray', 'TTuple']
    incorrect_types_with_call = ['TList()', 'TArray()', 'TTuple()']

    for d in _decorators:
        for t in _artiq_types:
            assert not evaluate(source.format(d, t))
        for t in incorrect_types:
            assert (3, 0, Error.INCORRECT_TYPE_USE) in evaluate(source.format(d, t))
        for t in incorrect_types_with_call:
            # These types are incorrect, but we decided not to go further into type checking
            assert not evaluate(source.format(d, t))
