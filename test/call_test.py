from flake8_artiq.errors import Error

from .tools import evaluate

_functions = ['delay(1 * s)', 'delay_mu(300)', 'at_mu(400)', 'now_mu()']


def test_timing_function_outside_kernel() -> None:
    source = """
    def foo(self):
        {}
    """
    for fn in _functions:
        assert (3, 4, Error.TIME_FN_OUTSIDE_KERNEL) in evaluate(source.format(fn))
    for fn in ['foo.{}'.format(f) for f in _functions] + ['foo.bar.{}'.format(f) for f in _functions]:
        # Only timing functions in the global namespace should raise an error
        assert not evaluate(source.format(fn))


def test_timing_function_outside_kernel_decorated() -> None:
    source = """
    @{}
    def foo(self):
        {}
    """
    decorators = ['staticmethod', 'classmethod',
                  'portable', 'portable(flags={"fast-math"})', 'host_only', 'rpc', 'rpc(flags={"async"})']
    for d in decorators:
        for fn in _functions:
            s = source.format(d, fn)
            assert (4, 4, Error.TIME_FN_OUTSIDE_KERNEL) in evaluate(s)


def test_timing_function_in_kernel() -> None:
    source = """
    @{}
    def foo(self):
        {}
    """
    decorators = ['kernel', 'kernel(flags={"fast-math"})']
    for d in decorators:
        for fn in _functions:
            assert not evaluate(source.format(d, fn))


def test_delay_signature() -> None:
    source = """
    @{}
    def foo(self):
        {}
    """
    decorators = ['kernel', 'kernel(flags={"fast-math"})']
    for d in decorators:
        for fn in ['delay()', 'delay(a, b)', 'delay_mu()', 'delay_mu(1, 1)']:
            s = source.format(d, fn)
            assert (4, 4, Error.DELAY_SIGNATURE) in evaluate(s)


def test_delay_const_arg() -> None:
    source = """
    @{}
    def foo(self):
        {}
    """
    decorators = ['kernel', 'kernel(flags={"fast-math"})']
    for d in decorators:
        for fn in ['delay(1.0)', 'delay(1)', 'delay("a")']:
            assert {(4, 4, Error.DELAY_CONST_ARG)} == evaluate(source.format(d, fn))
        for fn in ['delay(1 * s)', 'delay(duration)', 'delay(1+1)', 'delay(s)']:
            assert not evaluate(source.format(d, fn))
