from unittest.mock import Mock

import flake8.options.manager  # type: ignore[import-untyped]

import flake8_artiq.plugin
import flake8_artiq.errors

from .tools import evaluate, DEFAULT_OPTIONS


def test_empty() -> None:
    assert not evaluate('')


def test_error_types() -> None:
    for e in flake8_artiq.errors.Error:
        assert isinstance(e.value, tuple)
        assert isinstance(e.code, int)
        assert isinstance(e.msg, str)


def test_unique_error_codes() -> None:
    assert len(flake8_artiq.errors.Error) == len({e.code for e in flake8_artiq.errors.Error})


def test_valid_error_codes() -> None:
    assert all(0 <= e.code < 1000 for e in flake8_artiq.errors.Error)


def test_unique_error_messages() -> None:
    assert len(flake8_artiq.errors.Error) == len({e.msg for e in flake8_artiq.errors.Error})


def test_add_options() -> None:
    mock = Mock(spec=flake8.options.manager.OptionManager)
    flake8_artiq.plugin.ArtiqPlugin.add_options(mock)
    assert len(mock.mock_calls) == len(DEFAULT_OPTIONS)
