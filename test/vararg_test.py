from flake8_artiq.errors import Error

from .tools import evaluate


def test_vararg_kwarg() -> None:
    decorators = ['rpc', 'host_only']
    source = """
    @{}
    def foo(self, {}):
        pass
    """
    args = ['*args', 'a, b, *args', 'a, b, *args, c']
    for d in decorators:
        for a in args:
            assert not evaluate(source.format(d, a))


def test_vararg_in_kernel_def() -> None:
    decorators = ['kernel', 'portable']
    source = """
    @{}
    def foo(self, {}):
        pass
    """
    args = ['*args', 'a, b, *args', 'a, b, *args, c']
    for d in decorators:
        for a in args:
            assert {(3, 0, Error.VARARG)} == evaluate(source.format(d, a))


def test_kwarg_in_kernel_def() -> None:
    decorators = ['kernel', 'portable']
    source = """
    @{}
    def foo(self, {}):
        pass
    """
    args = ['**kwargs', 'a, b, **kwargs']
    for d in decorators:
        for a in args:
            assert {(3, 0, Error.KWARG)} == evaluate(source.format(d, a))
