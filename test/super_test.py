from flake8_artiq.errors import Error

from .tools import evaluate


def test_using_super() -> None:
    decorators = ['rpc', 'host_only']
    source = """
    @{}
    def foo(self):
        super().foo()
    """
    for d in decorators:
        assert not evaluate(source.format(d))


def test_using_super_in_kernel() -> None:
    decorators = ['kernel', 'portable']
    source = """
    @{}
    def foo(self):
        super().foo()
    """
    for d in decorators:
        assert {(4, 4, Error.USING_SUPER)} == evaluate(source.format(d))
