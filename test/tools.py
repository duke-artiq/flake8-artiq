import ast
import textwrap
import typing
import argparse

import flake8_artiq.plugin
import flake8_artiq.errors

__all__ = ['DEFAULT_OPTIONS', 'evaluate']


DEFAULT_OPTIONS = {
    'implicit_rpc': False,
    'no_untyped_args': False,
    'no_untyped_return': False,
}
"""Default plugin options."""


def evaluate(source: str, **kwargs: typing.Any) -> typing.Set[typing.Tuple[int, int, flake8_artiq.errors.Error]]:
    """Dedent and evaluate source, returns a set of error tuples (line, col, :class:`flake8_artiq.errors.Error`).

    :param source: The source code to evaluate
    :param kwargs: Additional options that will be parsed by the plugin, updates default options
    """
    assert isinstance(source, str)

    # Dedent source and parse
    tree = ast.parse(textwrap.dedent(source))
    # Create the plugin
    plugin = flake8_artiq.plugin.ArtiqPlugin(tree)
    # Process options
    options = DEFAULT_OPTIONS.copy()
    options.update(kwargs)
    plugin.parse_options(None, argparse.Namespace(**options), None)
    # Return processed error tuples as a set
    return {(line, col, flake8_artiq.errors.msg_to_error(msg)) for line, col, msg, _ in plugin.run()}
