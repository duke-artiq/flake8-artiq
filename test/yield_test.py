from flake8_artiq.errors import Error

from .tools import evaluate


def test_using_yield() -> None:
    decorators = ['rpc', 'host_only']
    source = """
    @{}
    def foo(self):
        yield True
    """
    for d in decorators:
        assert not evaluate(source.format(d))


def test_using_yield_in_kernel() -> None:
    decorators = ['kernel', 'portable']
    source = """
    @{}
    def foo(self):
        yield self.some_list
    """
    for d in decorators:
        assert {(4, 4, Error.USING_YIELD)} == evaluate(source.format(d))


def test_using_yield_from() -> None:
    decorators = ['rpc', 'host_only']
    source = """
    @{}
    def foo(self):
        yield True
    """
    for d in decorators:
        assert not evaluate(source.format(d))


def test_using_yield_from_in_kernel() -> None:
    decorators = ['kernel', 'portable']
    source = """
    @{}
    def foo(self):
        yield from self.some_list
    """
    for d in decorators:
        assert {(4, 4, Error.USING_YIELD_FROM)} == evaluate(source.format(d))
