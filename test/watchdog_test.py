from flake8_artiq.errors import Error

from .tools import evaluate


def test_watchdog() -> None:
    decorators = ['rpc', 'host_only', 'kernel', 'portable']
    source = """
    @{}
    def foo(self):
        with watchdog(10 * ms):
            pass
    """
    for d in decorators:
        assert {(4, 4, Error.USING_WATCHDOG)} == evaluate(source.format(d))
