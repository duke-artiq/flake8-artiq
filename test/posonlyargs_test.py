from flake8_artiq.errors import Error

from .tools import evaluate


def test_pos_only_args() -> None:
    source = """
    @{}
    def foo(self, a, /):
        pass
    """
    for d in ['rpc', 'host_only']:
        assert not evaluate(source.format(d))

    for d in ['kernel', 'portable']:
        assert {(3, 0, Error.POS_ONLY_ARGS)} == evaluate(source.format(d))
