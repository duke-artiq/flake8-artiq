from .tools import evaluate


def test_using_assert() -> None:
    # ARTIQ 5 supports assert statements, and aborts kernel execution
    # ARTIQ 6 raises an exception for failed assert statements
    decorators = ['rpc', 'host_only', 'kernel', 'portable']
    source = """
    @{}
    def foo(self):
        assert True
    """
    for d in decorators:
        assert not evaluate(source.format(d))
