import enum
import typing  # noqa:F401

__all__ = ['Error', 'msg_to_error']


@enum.unique
class Error(enum.Enum):
    # ARTIQ timing constructs
    TIME_FN_OUTSIDE_KERNEL = (101, 'timing function used outside of kernel')
    TIME_CX_OUTSIDE_KERNEL = (102, 'timing context used outside of kernel')
    COMBINED_TIME_CX = (103, 'more than one timing context in a single `with` statement')

    # ARTIQ decorators
    MULTIPLE_DECORATORS = (201, 'more than one ARTIQ decorator for one function')

    # ARTIQ kernel definitions
    ARG_TYPING = (301, 'used non-ARTIQ typing for function argument')
    RETURN_TYPING = (302, 'used non-ARTIQ typing for function return value')
    VARARG = (303, 'used variadic arguments for kernel function')
    KWARG = (304, 'used variadic keyword arguments for kernel function')
    POS_ONLY_ARGS = (305, 'used positional only arguments for kernel function')
    UNTYPED_ARGS = (306, 'untyped function arguments')
    UNTYPED_RETURN = (307, 'untyped function return value')
    INCORRECT_TYPE_USE = (308, 'incorrect use of ARTIQ type')
    MIXED_TUPLE_TYPE = (309, 'mixed tuple type can cause compilation error (see artiq#1671)')  # Disabled

    # ARTIQ kernel body
    USING_ASSERT = (401, 'using `assert` in a kernel is not supported')  # Disabled
    USING_YIELD = (402, 'using `yield` in a kernel is not supported')
    USING_YIELD_FROM = (403, 'using `yield from` in a kernel is not supported')
    DELAY_SIGNATURE = (404, '`delay()` and `delay_mu()` expect exactly one argument')
    DELAY_CONST_ARG = (405, 'using `delay()` with ambiguous constant argument')
    USING_SUPER = (406, 'using `super()` in a kernel is not supported')

    # Deprecated features
    USING_WATCHDOG = (801, 'using deprecated watchdog feature, which will be removed in ARTIQ 6 (see artiq#1458)')

    # Others
    MULTI_ITEM_WITH = (901, '`with` statement with multiple items (see artiq#1478)')
    USING_INTERLEAVE = (902, 'using `interleave`, which has limited documentation')
    IMPLICIT_SEQUENTIAL = (903, 'implicit sequential timing in shallow parallel block (see artiq#1555)')

    @property
    def code(self) -> int:
        """The error code."""
        code: int = self.value[0]
        return code

    @property
    def msg(self) -> str:
        """The error message."""
        msg: str = self.value[1]
        return msg


_CODE_TO_ERROR: typing.Dict[int, Error] = {e.code: e for e in Error}
"""Dict to convert error codes to error enums."""


def msg_to_error(msg: str) -> Error:
    """Convert an error message back to an :class:`Error`."""
    code = int(msg[3:7])
    return _CODE_TO_ERROR[code]
