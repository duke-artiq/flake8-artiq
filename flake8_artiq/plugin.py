import ast
import enum
import typing
import argparse
import flake8  # type: ignore[import-untyped]

from flake8_artiq.errors import Error
from flake8_artiq import __version__ as _plugin_version, __plugin_name__ as _plugin_name

__all__ = ['ArtiqPlugin']

_logger = flake8.LOG.getChild(_plugin_name)
"""The plugin logger."""

# Node type
_NODE_T = typing.Union[ast.stmt, ast.expr]
# The type of an ID
_ID_T = typing.Any


class NoID:
    """Singleton for no ID."""
    pass


def _get_id(node: ast.AST) -> _ID_T:
    """Recursively resolve the ID of a node.

    :param node: The node to resolve
    :returns: The ID, which is mostly a string but can also be an actual value for constants
    """
    if isinstance(node, ast.Name):
        return node.id
    elif isinstance(node, ast.Attribute):
        return '{}.{}'.format(_get_id(node.value), node.attr)
    elif isinstance(node, ast.Constant):
        return _get_id(node.value) if isinstance(node.value, ast.AST) else node.value
    elif isinstance(node, ast.Call):
        return _get_id(node.func)
    elif isinstance(node, ast.withitem):
        return _get_id(node.context_expr)
    elif isinstance(node, ast.Subscript):
        return _get_id(node.value)
    elif isinstance(node, (ast.FunctionDef, ast.ClassDef)):
        return node.name
    elif isinstance(node, ast.arg):
        return node.arg
    else:
        # Emit a warning and return no ID
        _logger.error('Can not get ID of type {}'.format(type(node)))
        return NoID


@enum.unique
class _TimeContextType(enum.Enum):
    """Enumeration for time context types."""

    SEQUENTIAL = 'sequential'
    PARALLEL = 'parallel'
    INTERLEAVE = 'interleave'
    IMPLICIT_SEQUENTIAL = 'implicit_sequential'  # Added when we enter an implicit sequential block

    def __str__(self) -> str:
        v: str = self.value
        return v


_STR_TO_TIME_CONTEXT_TYPE = {str(c): c for c in _TimeContextType if c is not _TimeContextType.IMPLICIT_SEQUENTIAL}
"""Dict to convert strings to a time context type."""

_TIMELESS_GLOBALS: typing.Set[str] = {
    # Value constructors (includes NumPy constructors)
    'bool', 'int', 'float', 'str', 'bytes', 'bytearray', 'list', 'array', 'range', 'int32', 'int64',
    # Exception constructors
    'Exception', 'IndexError', 'ValueError', 'ZeroDivisionError', 'RuntimeError',
    # Build-in Python functions
    'len', 'round', 'abs', 'min', 'max', 'print',
    # ARTIQ utility functions
    'rtio_log', 'core_log',
}
"""Functions in the ARTIQ global namespace that do not affect the timeline."""


class _TimeContext:
    """Data class for a time context."""

    def __init__(self, type_: _TimeContextType):
        assert isinstance(type_, _TimeContextType)

        # Store the type
        self._type = type_
        # The number times this context was entered (stack compression)
        self._num_enter = 1
        # Keep a counter for the number of calls
        self._call_count = 0

    @property
    def type(self) -> _TimeContextType:
        """The type of this context."""
        return self._type

    @property
    def num_entered(self) -> int:
        """The number of times this context was entered."""
        return self._num_enter

    @property
    def num_calls(self) -> int:
        """The number of calls that were registered and could have changed the timeline."""
        return self._call_count

    def enter(self) -> None:
        """Enter this context."""
        self._num_enter += 1

    def exit(self) -> None:
        """Exit this context."""
        self._num_enter -= 1
        assert self._num_enter > 0, 'Context was exited too many times'

    def pop_to_exit(self) -> bool:
        """True if this context needs to be popped of the stack to exit."""
        return self._num_enter == 1

    def register_call(self, fn_id: _ID_T) -> None:
        """Register a call, will filter out calls that are not relevant."""
        if fn_id not in _TIMELESS_GLOBALS:
            self._call_count += 1


class _TimeContextStack:
    """Class for a time context stack."""

    def __init__(self) -> None:
        # Start with a single sequential context
        self._stack: typing.List[_TimeContext] = [_TimeContext(_TimeContextType.SEQUENTIAL)]

    @property
    def top(self) -> _TimeContext:
        """The top of the stack."""
        return self._stack[-1]

    def push(self, context_type: _TimeContextType) -> None:
        """Push a new context to the stack."""
        assert isinstance(context_type, _TimeContextType)

        if context_type is self.top.type:
            # Enter instead of pushing a new context (stack compression)
            self.top.enter()
        else:
            # Push a new context
            self._stack.append(_TimeContext(context_type))

    def pop(self) -> None:
        """Pop the last context off the stack."""

        if self.top.pop_to_exit():
            # Pop the context
            self._stack.pop()
        else:
            # Exit instead of popping the object (stack compression)
            self.top.exit()

    def __len__(self) -> int:
        """The size of the stack."""
        return sum(context.num_entered for context in self._stack)


class _FunctionDef:
    """Data class for a function definition."""

    def __init__(self, node: ast.FunctionDef):
        assert isinstance(node, ast.FunctionDef)

        # Store the node
        self._node = node
        # Extract a set of decorators
        self._decorators = {_get_id(d) for d in self._node.decorator_list}

    @property
    def decorators(self) -> typing.Set[_ID_T]:
        """The decorators of this function definition."""
        return self._decorators

    @property
    def has_return_type(self) -> bool:
        """True if the function definition has a return type annotation."""
        return self._node.returns is not None


_TIME_CX: typing.Set[str] = {'sequential', 'parallel', 'interleave'}
"""ARTIQ time contexts."""
_DELAY_FN: typing.Set[str] = {'delay', 'delay_mu'}
"""ARTIQ delay functions."""
_TIME_FN: typing.Set[str] = _DELAY_FN | {'at_mu', 'now_mu'}
"""ARTIQ time functions."""

_DECORATORS: typing.Set[str] = {'kernel', 'portable', 'rpc', 'host_only'}
"""ARTIQ decorators."""

_PARTIAL_TYPES: typing.Set[str] = {'TTuple', 'TList', 'TArray'}
"""Partial ARTIQ types require an item type definition to be complete."""
_TYPES: typing.Set[str] = {
    'TNone', 'TTuple',
    'TBool', 'TInt32', 'TInt64', 'TFloat',
    'TStr', 'TBytes', 'TByteArray',
    'TList', 'TArray', 'TRange32', 'TRange64',
    'TVar',
}
"""All ARTIQ types."""


class Visitor(ast.NodeVisitor):
    """Node visitor class."""

    def __init__(self, *,
                 implicit_rpc: bool = False,
                 no_untyped_args: bool = False,
                 no_untyped_return: bool = False
                 ) -> None:
        assert isinstance(implicit_rpc, bool)
        assert isinstance(no_untyped_args, bool)
        assert isinstance(no_untyped_return, bool)

        # Store configuration
        self._implicit_rpc = implicit_rpc
        self._no_untyped_args = no_untyped_args
        self._no_untyped_return = no_untyped_return

        # The list that collects all errors found
        self._errors: typing.List[typing.Tuple[int, int, Error]] = []
        # The function definition stack
        self._function_stack: typing.List[_FunctionDef] = []
        # The time context stack
        self._time_stack = _TimeContextStack()

    """Visit functions"""

    def visit_FunctionDef(self, node: ast.FunctionDef) -> None:
        # Push function definition to the stack
        self._function_stack.append(_FunctionDef(node))

        # Count the number of ARTIQ decorators
        num_artiq_decorators = sum(_get_id(d) in _DECORATORS for d in node.decorator_list)
        # Check ARTIQ decorators
        if num_artiq_decorators > 1:
            self._add_error(node, Error.MULTIPLE_DECORATORS)

        if self._no_untyped_args:
            # Extract arguments
            args: typing.List[typing.Any] = node.args.args
            if args and _get_id(args[0]) == 'self':
                # Remove argument 'self'
                args = args[1:]

            # Check for untyped arguments
            if (self._in_kernel() or self._in_portable()) and any(arg.annotation is None for arg in args):
                self._add_error(node, Error.UNTYPED_ARGS)

        # Check typing
        if self._in_kernel() or self._in_portable():
            arg_types = {_get_id(arg.annotation): arg for arg in node.args.args if arg.annotation is not None}
            if any(ty not in _TYPES for ty in arg_types):
                self._add_error(node, Error.ARG_TYPING)
            for arg in (a for t, a in arg_types.items() if t in _PARTIAL_TYPES):
                if not isinstance(arg.annotation, ast.Call):
                    self._add_error(node, Error.INCORRECT_TYPE_USE)
                    break  # Only add one error
        if self._in_kernel() or self._in_portable() or self._in_rpc():
            if node.returns is not None:
                ret_type = _get_id(node.returns)
                if ret_type not in _TYPES:
                    self._add_error(node, Error.RETURN_TYPING)
                elif ret_type in _PARTIAL_TYPES and not isinstance(node.returns, ast.Call):
                    self._add_error(node, Error.INCORRECT_TYPE_USE)

        # Check for variable and positional-only arguments
        if self._in_kernel() or self._in_portable():
            if node.args.vararg is not None:
                self._add_error(node, Error.VARARG)
            if node.args.kwarg is not None:
                self._add_error(node, Error.KWARG)
            if node.args.posonlyargs:
                self._add_error(node, Error.POS_ONLY_ARGS)

        # Push a sequential time context when entering a function definition
        self._time_stack.push(_TimeContextType.SEQUENTIAL)
        # Continue descend
        self.generic_visit(node)
        # Pop time context from stack
        self._time_stack.pop()

        # Pop function decorators from the stack
        self._function_stack.pop()

    def visit_With(self, node: ast.With) -> None:
        # Extract context ID's
        contexts = [_get_id(i) for i in node.items]
        contexts_set = set(contexts)
        time_contexts = [c for c in contexts if c in _TIME_CX]

        # Check contexts
        if time_contexts and not self._in_kernel():
            self._add_error(node, Error.TIME_CX_OUTSIDE_KERNEL)
        if len(time_contexts) > 1:
            self._add_error(node, Error.COMBINED_TIME_CX)
        if len(contexts) > 1 and self._in_kernel():
            self._add_error(node, Error.MULTI_ITEM_WITH)
        if 'interleave' in contexts_set:
            self._add_error(node, Error.USING_INTERLEAVE)
        if 'watchdog' in contexts_set:
            self._add_error(node, Error.USING_WATCHDOG)

        if time_contexts:
            # Push the last time context
            self._time_stack.push(_STR_TO_TIME_CONTEXT_TYPE[time_contexts[-1]])
        else:
            # Push an implicit sequential time context
            self._push_implicit_sequential()

        # Continue descend
        self.generic_visit(node)
        # Pop time context from stack
        self._time_stack.pop()

    def visit_Call(self, node: ast.Call) -> None:
        # Check if the call is a super call
        if _get_id(node) == 'super' and (self._in_kernel() or self._in_portable()):
            self._add_error(node, Error.USING_SUPER)

        # Get function id
        func = _get_id(node)

        # Check calls
        if func in _TIME_FN and not self._in_kernel():
            self._add_error(node, Error.TIME_FN_OUTSIDE_KERNEL)
        if func in _DELAY_FN:
            if len(node.args) != 1:
                self._add_error(node, Error.DELAY_SIGNATURE)
            else:
                if func == 'delay' and isinstance(node.args[0], ast.Constant):
                    self._add_error(node, Error.DELAY_CONST_ARG)

        # Register this call in the time stack
        self._time_stack.top.register_call(func)
        # Check for implicit sequential timing
        if self._time_stack.top.type is _TimeContextType.IMPLICIT_SEQUENTIAL and self._time_stack.top.num_calls > 1:
            self._add_error(node, Error.IMPLICIT_SEQUENTIAL)

        # Continue descend
        self._implicit_sequential_visit(node)

    def visit_Yield(self, node: ast.Yield) -> None:
        # Check for usage of yield in kernels
        if self._in_kernel() or self._in_portable():
            self._add_error(node, Error.USING_YIELD)

        # Continue descend
        self._implicit_sequential_visit(node)

    def visit_YieldFrom(self, node: ast.YieldFrom) -> None:
        # Check for usage of yield from in kernels
        if self._in_kernel() or self._in_portable():
            self._add_error(node, Error.USING_YIELD_FROM)

        # Continue descend
        self._implicit_sequential_visit(node)

    def visit_If(self, node: ast.If) -> None:
        # Visit test and body as one trace
        self._push_implicit_sequential()
        self._generic_visit_field(node.test)
        self._generic_visit_field(node.body)
        self._time_stack.pop()

        # Visit test and else-body as the other trace
        self._push_implicit_sequential()
        self._generic_visit_field(node.test)
        self._generic_visit_field(node.orelse)
        self._time_stack.pop()

    def visit_Return(self, node: ast.Return) -> None:
        if self._no_untyped_return:
            has_return_value = node.value is not None and _get_id(node.value) is not None
            if has_return_value and (self._in_kernel() or self._in_portable() or self._in_rpc()):
                # Check for untyped return in function definition
                if not self._function_stack[-1].has_return_type:
                    self._add_error(node, Error.UNTYPED_RETURN)

        # Continue descend
        self._implicit_sequential_visit(node)

    def visit_Assign(self, node: ast.Assign) -> None:
        self._implicit_sequential_visit(node)

    def visit_AugAssign(self, node: ast.AugAssign) -> None:
        self._implicit_sequential_visit(node)

    def visit_Assert(self, node: ast.Assert) -> None:
        self._implicit_sequential_visit(node)

    def visit_For(self, node: ast.For) -> None:
        self._implicit_sequential_visit(node)

    def visit_While(self, node: ast.While) -> None:
        self._implicit_sequential_visit(node)

    def visit_Try(self, node: ast.Try) -> None:
        self._implicit_sequential_visit(node)

    def visit_Raise(self, node: ast.Raise) -> None:
        self._implicit_sequential_visit(node)

    def visit_Expr(self, node: ast.Expr) -> None:
        self._implicit_sequential_visit(node)

    """Helper functions"""

    def _add_error(self, node: _NODE_T, error: Error) -> None:
        """Add an error to the error list."""
        self._errors.append((node.lineno, node.col_offset, error))

    def _push_implicit_sequential(self) -> None:
        """Add an implicit sequential time context as a result of entering a statement."""
        if self._time_stack.top.type in {_TimeContextType.IMPLICIT_SEQUENTIAL, _TimeContextType.PARALLEL}:
            # Push another implicit sequential time context
            self._time_stack.push(_TimeContextType.IMPLICIT_SEQUENTIAL)
        else:
            # Push a regular sequential time context
            self._time_stack.push(_TimeContextType.SEQUENTIAL)

    def _generic_visit_field(self, field: typing.Any) -> None:
        """Generic visit function for fields of a node.

        This function is a variation of the :func:`generic_visit` function that accepts separate fields.

        :param field: The field object to visit
        """
        if isinstance(field, list):
            # Visit items recursively
            for item in field:
                self._generic_visit_field(item)
        elif isinstance(field, ast.AST):
            # Visit the field
            self.visit(field)

    def _implicit_sequential_visit(self, node: ast.AST) -> None:
        """Visit a node in an implicit sequential context.

        :param node: The node object to visit
        """
        # Push an implicit sequential time context
        self._push_implicit_sequential()
        # Continue descend
        self.generic_visit(node)
        # Pop time context from stack
        self._time_stack.pop()

    def _in_kernel(self) -> bool:
        """True if the current node is inside a kernel function."""
        if self._function_stack:
            return 'kernel' in self._function_stack[-1].decorators
        else:
            return False  # Not in a function def

    def _in_portable(self) -> bool:
        """True if the current node is inside a portable function."""
        if self._function_stack:
            return 'portable' in self._function_stack[-1].decorators
        else:
            return False  # Not in a function def

    def _in_rpc(self) -> bool:
        """True if the current node is inside an RPC function."""
        if self._function_stack:
            explicit_rpc = 'rpc' in self._function_stack[-1].decorators
            if self._implicit_rpc:
                return explicit_rpc or not self._function_stack[-1].decorators & _DECORATORS
            else:
                return explicit_rpc
        else:
            return False  # Not in a function def

    def _in_host_only(self) -> bool:
        """True if the current node is inside a host only function."""
        if self._function_stack:
            return 'host_only' in self._function_stack[-1].decorators
        else:
            return False  # Not in a function def

    def errors(self) -> typing.Generator[typing.Tuple[int, int, Error], None, None]:
        """Returns an iterator of tuples (line, col, Error)."""
        yield from self._errors


class ArtiqPlugin:
    """The flake8 ARTIQ plugin class."""

    name: str = _plugin_name
    """The name of this plugin as shown in flake8."""
    version: str = _plugin_version
    """The version of this plugin as shown in flake8."""

    _ERR_FORMAT: str = 'ATQ{code:03d} {msg}'
    """Format of error messages."""

    _implicit_rpc: bool = False
    """Class variable for passing the implicit RPC option."""
    _no_untyped_args: bool = False
    """Class variable for passing the no untyped arguments option."""
    _no_untyped_return: bool = False
    """Class variable for passing the no untyped return option."""

    def __init__(self, tree: ast.AST):
        """Initialize this plugin.

        :param tree: The AST to parse
        """
        assert isinstance(tree, ast.AST)

        # The abstract syntax tree
        self._tree = tree

    @classmethod
    def add_options(cls, option_manager: typing.Any) -> None:
        """Add options, called by flake8."""
        # Implicit RPC option
        option_manager.add_option('--implicit-rpc', default=False, action='store_true', parse_from_config=True,
                                  help='Check functions without an ARTIQ decorator as RPC functions')
        # Typing options
        option_manager.add_option('--no-untyped-args', default=False, action='store_true', parse_from_config=True,
                                  help='Do not allow untyped arguments for kernel and portable functions')
        option_manager.add_option('--no-untyped-return', default=False, action='store_true', parse_from_config=True,
                                  help='Do not allow untyped return value for kernel, portable, and RPC functions')

    @classmethod
    def parse_options(cls, _option_manager: typing.Any, options: argparse.Namespace, _args: typing.Any) -> None:
        """Parse options, called by flake8."""
        # Store options
        cls._implicit_rpc = options.implicit_rpc
        _logger.debug('Implicit RPC: {}'.format(cls._implicit_rpc))
        cls._no_untyped_args = options.no_untyped_args
        cls._no_untyped_return = options.no_untyped_return
        _logger.debug('No untyped args/return: {}/{}'.format(cls._no_untyped_args, cls._no_untyped_return))

    def run(self) -> typing.Generator[typing.Tuple[int, int, str, typing.Type[typing.Any]], None, None]:
        """The run plugin, called by flake8."""
        # Visit the tree
        visitor = Visitor(
            implicit_rpc=self._implicit_rpc,
            no_untyped_args=self._no_untyped_args,
            no_untyped_return=self._no_untyped_return
        )
        visitor.visit(self._tree)

        # Yield errors
        for line, col, error in visitor.errors():
            yield line, col, self._ERR_FORMAT.format(code=error.code, msg=error.msg), self.__class__
