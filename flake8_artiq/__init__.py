__plugin_name__: str = 'flake8-artiq'
"""The (unique) name of this flake8 plugin."""

__version__: str = '8'
"""The version of this plugin."""
